#!/usr/bin/python3

import configparser
import socket
import ssl


class Config:

	def __init__(self):
		self._config = configparser.ConfigParser()
		self._config.read('server.ini')

	@property
	def address_family(self):
		af = self._config['client']['family']
		if af == 'inet':
			return socket.AF_INET
		elif af == 'inet6':
			return socket.AF_INET6
		else:
			raise ValueError("{} is not a valid address family.".format(af))

	@property
	def cert_path(self):
		return self._config['client']['cert']

	@property
	def key_path(self):
		return self._config['client']['key']

	@property
	def bind_address(self):
		af = self._config['client']['family']
		if af == 'inet' or af == 'inet6':
			return self._config['client']['address'], int(self._config['client']['port'])

	@property
	def server_address(self):
		return self._config['server']['address'], int(self._config['server']['port'])

	@property
	def bufsize(self):
		return int(self._config['client']['buffer'])


if __name__ == '__main__':
	config = Config()
	context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
	context.load_cert_chain(config.cert_path, config.key_path)
	rawsock = socket.socket(config.address_family, socket.SOCK_STREAM)
	rawsock.bind(config.bind_address)
	rawsock.listen(5)
	with context.wrap_socket(rawsock, server_side=True) as ssock:
		conn, addr = ssock.accept()
		with socket.socket() as sock:
			sock.connect(config.server_address)
			sock.send(ssock.recv(config.bufsize))
