#!/usr/bin/python3

import configparser
import socket
import ssl


class Config:

	def __init__(self):
		self._config = configparser.ConfigParser()
		self._config.read('client.ini')

	@property
	def address_family(self):
		af = self._config['client']['family']
		if af == 'inet':
			return socket.AF_INET
		elif af == 'inet6':
			return socket.AF_INET6
		elif af == 'unix':
			return socket.AF_UNIX
		else:
			raise ValueError("{} is not a valid address family.".format(af))

	@property
	def bind_address(self):
		af = self._config['client']['family']
		if af == 'inet' or af == 'inet6':
			return self._config['client']['address'], int(self._config['client']['port'])
		elif af == 'unix':
			return self._config['client']['address']

	@property
	def server_address(self):
		return self._config['server']['address'], int(self._config['server']['port'])

	@property
	def server_hostname(self):
		return self._config['server']['hostname']

	@property
	def bufsize(self):
		return int(self._config['client']['buffer'])


if __name__ == '__main__':
	config = Config()
	with socket.socket(config.address_family, socket.SOCK_STREAM) as sock:
		sock.bind(config.bind_address)
		sock.listen(1)
		conn, addr = sock.accept()
		context = ssl.SSLContext()
		rawsock = socket.socket()
		rawsock.connect(config.server_address)
		with context.wrap_socket(rawsock, server_hostname=config.server_hostname) as ssock:
			while True:
				ssock.send(sock.recv(config.bufsize))
