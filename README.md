# TLS Relay

Pour faire passer une connexion en clair à travers un tunnel TLS.

## Dépendances

 * `python` (>=3.5)

## Configuration

### client.ini
```ini
[client]
; Famille de l'adresse du client (inet, inet6 ou unix)
family = inet
; Adresse du client : IPv4, IPv6 ou chemin (en fonction de family)
address = 127.0.0.1
; Port (pour inet et inet6)
port = 4240
buffer = 4096

[server]
; Nom de domaine du serveur
hostname = localhost
; Adresse IP du serveur
address = 127.0.0.1
port = 4241
```

### server.ini
```ini
[client]
family = inet
; Adresse d'écoute du serveur
address = 0.0.0.0
; Port d'acoute
port = 4241
buffer = 4096
; Chemins du certificat public et de la clef privée
cert = cert.pem
key = privkey.pem

[server]
; Adresse du serveur local
address = 127.0.0.1
port = 4242
```

## Utilisation

Lancer `server.py` sur le serveur distant et `client.py` en local.